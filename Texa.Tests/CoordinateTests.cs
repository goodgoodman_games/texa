﻿using System.Linq;
using NUnit.Framework;
using Texa.Map;

namespace Texa.Tests
{
    [TestFixture]
    public class CoordinateTests
    {
        private static readonly Coordinate[] DIRECTIONS =
        {
            new Coordinate(1, 0),
            new Coordinate(0, 1),
            new Coordinate(-1, 1),
            new Coordinate(1, -1),
            new Coordinate(-1, 0),
            new Coordinate(0, -1),
        };

        [Test]
        public void EqualTest()
        {
            var origin = new Coordinate(0, 0);
            var same = new Coordinate(0, 0);
            var other = new Coordinate(1, 0);

            Assert.That(origin == same, Is.True);
            Assert.That(origin != same, Is.Not.True);
            
            Assert.That(same.GetHashCode(), Is.EqualTo(origin.GetHashCode()));
            Assert.That(other.GetHashCode(), Is.Not.EqualTo(origin.GetHashCode()));
            
            Assert.That(origin.Equals((object) same), Is.True);
            Assert.That(origin.Equals(null), Is.Not.True);
            
            Assert.That(origin.ToString(), Is.EqualTo($"{{X : {origin.X}, Y : {origin.Y}, Z : {origin.Z}}}"));
        }
        
        [Test]
        public void AddVectorTest()
        {
            var origin = new Coordinate(0, 0);

            var resultPairs = DIRECTIONS.Select(direction => new {Direction = direction, Result = direction + origin});
            foreach (var resultPair in resultPairs)
                Assert.That(resultPair.Result, Is.EqualTo(resultPair.Result));
        }
        
        [Test]
        public void DistanceTest()
        {
            var from = new Coordinate(0, 0);
            {
                var to = DIRECTIONS[0];
                Assert.That(Coordinate.Distance(from, to), Is.EqualTo(1));
            }

            {
                var to = DIRECTIONS[0] + DIRECTIONS[0];
                Assert.That(Coordinate.Distance(from, to), Is.EqualTo(2));
            }

            {
                var to = DIRECTIONS[0] + DIRECTIONS[1];
                Assert.That(Coordinate.Distance(from, to), Is.EqualTo(2));
            }
        }
    }
}