using System;
using System.Collections.Generic;
using NUnit.Framework;
using Texa.Phase;

namespace Texa.Tests
{
    [TestFixture]
    public class PhaseProcessorTest
    {
        public enum TestKeys
        {
            Default,
            RegisterTest_1,
            RegisterTest_2,
            UnregisterTest_1,
            UnregisterTest_2,
            SetPhaseTest_1,
            SetPhaseTest_2
        }

        public class RegisterTest_1 : IPhase
        {
            public void OnEnterPhase() { }
            public void OnLeavePhase() { }
        }

        public class UnregisterTest_1 : IPhase
        {
            public void OnEnterPhase() { }
            public void OnLeavePhase() { }
        }

        public class SetPhaseTest_1 : IPhase
        {
            public void OnEnterPhase() { }
            public void OnLeavePhase() { }
        }
        
        public class SetPhaseTest_2 : IPhase
        {
            public void OnEnterPhase() { }
            public void OnLeavePhase() { }
        }

        public class TestProcessor : PhaseProcessor<TestKeys>
        {
            private IPhase _currentPhase;
            private readonly Dictionary<TestKeys, IPhase> _phases = new Dictionary<TestKeys, IPhase>();

            public void RegisterPhase(TestKeys key, IPhase phase) => Register(key, phase);
            public void UnregisterPhase(TestKeys key) => Unregister(key);
        }

        private TestProcessor _processor = new TestProcessor();
        private TestKeys DefaultKey = TestKeys.Default;
        private TestKeys RegisterTestKey_1 = TestKeys.RegisterTest_1;
        private TestKeys RegisterTestKey_2 = TestKeys.RegisterTest_2;
        private TestKeys UnregisterTestKey_1 = TestKeys.UnregisterTest_1;
        private TestKeys UnregisterTestKey_2 = TestKeys.UnregisterTest_2;
        private TestKeys SetPhaseTestKey_1 = TestKeys.SetPhaseTest_1;
        private TestKeys SetPhaseTestKey_2 = TestKeys.SetPhaseTest_2;

        [Test]
        public void RegisterTest()
        {
            _processor.RegisterPhase(RegisterTestKey_1, new RegisterTest_1());
            _processor.SetPhase(RegisterTestKey_1);
            Assert.That(_processor.GetCurrentPhaseKey(), Is.EqualTo(RegisterTestKey_1));

            Assert.Throws<ArgumentException>(() => _processor.RegisterPhase(RegisterTestKey_1, new RegisterTest_1()));
            Assert.Throws<ArgumentException>(() => _processor.RegisterPhase(RegisterTestKey_1, null));
            Assert.Throws<ArgumentNullException>(() => _processor.RegisterPhase(DefaultKey, null));
        }

        [Test]
        public void UnRegisterTest()
        {
            _processor.RegisterPhase(UnregisterTestKey_1, new UnregisterTest_1());
            _processor.SetPhase(UnregisterTestKey_1);
            
            _processor.UnregisterPhase(UnregisterTestKey_1);
            Assert.That(_processor.GetCurrentPhaseKey(), Is.EqualTo(default(TestKeys)));

            Assert.Throws<ArgumentException>(() => _processor.UnregisterPhase(UnregisterTestKey_1));
            Assert.Throws<ArgumentException>(() => _processor.UnregisterPhase(UnregisterTestKey_2));
        }

        [Test]
        public void SetPhaseTest()
        {
            _processor.RegisterPhase(SetPhaseTestKey_1, new SetPhaseTest_1());
            _processor.RegisterPhase(SetPhaseTestKey_2, new SetPhaseTest_2());

            _processor.SetPhase(SetPhaseTestKey_1);
            Assert.That(_processor.GetCurrentPhaseKey(), Is.EqualTo(SetPhaseTestKey_1));

            _processor.SetPhase(SetPhaseTestKey_2);
            Assert.That(_processor.GetCurrentPhaseKey(), Is.EqualTo(SetPhaseTestKey_2));

            Assert.Throws<ArgumentException>(() => _processor.SetPhase(DefaultKey));
        }

        [Test]
        public void DisposeTest()
        {
            _processor.Dispose();
            Assert.Throws<ArgumentException>(() => _processor.SetPhase(DefaultKey));
        }

    }
}