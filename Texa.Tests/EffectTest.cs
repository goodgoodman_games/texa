using System;
using System.Collections.Generic;
using NUnit.Framework;
using Texa.Effect;

namespace Texa.Tests
{
    [TestFixture]
    public class EffectProcessorTests
    {
        private class TestGameStatus
        {
            public int Value { get; set; }

            public TestGameContext CreateContext() => new TestGameContext();
        }

        private class TestGameContext
        {
            
        }

        private class TestEffectProcessor : EffectProcessor<TestGameContext>
        {
            private class Result : EffectResult<TestGameStatus>
            {
                protected override void ApplyTo(TestGameStatus gameStatus) { gameStatus.Value += 3; }
                public override object ToResultInfo() { return new object(); }
            }

            public override IEnumerable<EffectResult> Invoke(TestGameContext context)
            {
                for(int i = 0; i < 3; i++)
                    yield return new Result();
            }
        }

        [Test]
        public void InvokeEffectTest()
        {
            var testGameStatus = new TestGameStatus();
            var testProcessor = new TestEffectProcessor();
            var results = testProcessor.Invoke(testGameStatus.CreateContext());

            int value = 0;
            Assert.That(testGameStatus.Value == value, Is.True);
            foreach (var result in results)
            {
                value += 3;
                
                result.ApplyTo(testGameStatus);
                
                Assert.Throws<ArgumentException>(() => result.ApplyTo(new object()));
                Assert.That(testGameStatus.Value == value, Is.True);
            }
        }
    }
}