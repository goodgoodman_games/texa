﻿using NUnit.Framework;
using Texa.Map;
using Texa.Map.Generator;

namespace Texa.Tests
{
    [TestFixture]
    public class GridTests
    {
        private static readonly Coordinate[] DIRECTIONS =
        {
            new Coordinate(1, 0),
            new Coordinate(0, 1),
            new Coordinate(-1, 1),
            new Coordinate(1, -1),
            new Coordinate(-1, 0),
            new Coordinate(0, -1),
        };

        public GridTests() { }

        private Grid _grid = new Grid(new ArenaGridGenerator(new GridConfig(10, 5)));

        [Test]
        public void GetNeighborTest()
        {
            // 구석에 끼지 않아 육방에 전부 유효한 좌표가 존재하는 경우
            {
                var neighbor = _grid.GetNeighbor(new Coordinate(0, 0));
                Assert.That(neighbor, Is.EquivalentTo(DIRECTIONS));
            }
        }

        [Test]
        public void GetTileInRange()
        {
            // 거리가 1인 타일들은 DIRECTIONS와 같다.
            {
                var tiles = _grid.GetTilesInRange(new Coordinate(0, 0), 1);
                Assert.That(tiles, Is.EquivalentTo(DIRECTIONS));
            }
        }
    }
}