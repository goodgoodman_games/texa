﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Texa.Effect;
using Texa.Map;
using Texa.Map.Generator;
using Texa.UDG.Effect.Context;
using Texa.UDG.Effect.Processor;
using Texa.UDG.Models;

namespace Texa.UDG.Tests
{
    [TestFixture]
    public class EffectProcessorTests
    {
        [SetUp]
        public void Initialize()
        {
            _gameStatus = new TestGameStatus
            {
                PlayerInfos = new List<IPlayerInfo>()
                {
                    new TestPlayerInfo(userID: 1)
                    {
                        Deck = new List<Card>() { new TestCard() {UniqueID = 1, ID = 1, HP = 2, Damage = 3, Name = "하하", Abilities = { } } },
                        Hands = new List<Card>()
                        {
                            new TestCard() {UniqueID = 2, ID = 2, HP = 2, Damage = 3, Name = "하하", Cost = 1, Abilities = { } },
                            new TestCard() {UniqueID = 3, ID = 3, HP = 2, Damage = 3, Name = "하하", Cost = 1, Abilities = { } },
                        },
                    }
                },
                
                Units = new List<Unit>
                {
                    new TestUnit(null, 1, 1) { HP = 2, Damage = 1, Coordinate = new Coordinate(0, 0), Abilities = new int[] { } },
                    new TestUnit(null, 2, 1) { HP = 2, Damage = 1, Coordinate = new Coordinate(1, 0), Abilities = new[] { 1 } },
                    new TestUnit(null, 3, 1) { HP = 2, Damage = 1, Coordinate = new Coordinate(3, 0), Abilities = new int[] { } },
                },
                
                Map = new Grid(new ArenaGridGenerator(new GridConfig(50, 50)))
            };
        }

        private TestGameStatus _gameStatus;
        
        [Test]
        public void DamageTargetUnitEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new DamageTargetUnitEffectProcessor();
            
            var wrongContext = _gameStatus.CreateContext(1, new Coordinate(-1, 0), 4, null, default(int));
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: new Coordinate(1, 0), 
                value: 4,
                targetCardIDs: null,
                targetUserID: default(int));
            
            var wrongResults = processor.Invoke(wrongContext);
            Assert.Throws<ArgumentException>(() => wrongResults.GetEnumerator().MoveNext());
            
            var rightResults = processor.Invoke(rightContext);
            
            var targetUnit = _gameStatus.Units.FirstOrDefault(unit => unit.Coordinate == new Coordinate(1, 0));
            Assert.That(targetUnit.HP, Is.EqualTo(2));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            Assert.That(targetUnit.HP, Is.EqualTo(-2));
        }
        
        [Test]
        public void DamageRangeUnitEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new DamageRangeUnitEffectProcessor();
            
            var wrongContext = _gameStatus.CreateContext(1, new Coordinate(-3, 0), 4, null, default(int));
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: new Coordinate(1, 0), 
                value: 4,
                targetCardIDs: null,
                targetUserID: default(int));
            
            var wrongResults = processor.Invoke(wrongContext);
            Assert.That(wrongResults.Count(), Is.EqualTo(0));
            
            var rightResults = processor.Invoke(rightContext);
            
            var targetUnits = _gameStatus.Units.Where(unit => Coordinate.Distance(unit.Coordinate, new Coordinate(1, 0)) == 1);
            
            foreach(var unit in targetUnits)
                Assert.That(unit.HP, Is.EqualTo(2));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            foreach(var unit in targetUnits)
                Assert.That(unit.HP, Is.EqualTo(-2));
        }
        
        [Test]
        public void DrawCardEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new DrawCardEffectProcessor();
            
            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), default(int), new [] { 1 }, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), default(int), new [] { 2 }, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate), 
                value: default(int),
                targetCardIDs: new [] { 1 },
                targetUserID: 1);
            
            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach(var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach(var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));
            
            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);
            Assert.That(targetInfo.Deck.FirstOrDefault(card => card.ID == 1), Is.Not.Null);
            Assert.That(targetInfo.Hands.FirstOrDefault(card => card.ID == 1), Is.Null);
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            Assert.That(targetInfo.Deck.FirstOrDefault(card => card.ID == 1), Is.Null);
            Assert.That(targetInfo.Hands.FirstOrDefault(card => card.ID == 1), Is.Not.Null);
        }
        
        [Test]
        public void HandCardIncreaseCostEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardIncreaseCostEffectProcessor();
            
            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new [] { 2, 3 }, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new [] { 1 }, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate), 
                value: 4,
                targetCardIDs: new [] { 2, 3 },
                targetUserID: 1);
            
            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach(var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach(var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));
            
            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);
            
            foreach(var card in targetInfo.Hands)
                Assert.That(card.Cost, Is.EqualTo(1));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            foreach(var card in targetInfo.Hands)
                Assert.That(card.Cost, Is.EqualTo(5));
        }
        
        [Test]
        public void HandCardDecreaseCostEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardDecreaseCostEffectProcessor();
            
            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new [] { 2, 3 }, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new [] { 1 }, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate), 
                value: 4,
                targetCardIDs: new [] { 2, 3 },
                targetUserID: 1);
            
            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach(var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach(var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));
            
            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);
            
            foreach(var card in targetInfo.Hands)
                Assert.That(card.Cost, Is.EqualTo(1));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            foreach(var card in targetInfo.Hands)
                Assert.That(card.Cost, Is.EqualTo(0));
        }
        
        [Test]
        public void HandCardChangeCostEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardChangeCostEffectProcessor();
            
            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new [] { 2, 3 }, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new [] { 1 }, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate), 
                value: 4,
                targetCardIDs: new [] { 2, 3 },
                targetUserID: 1);
            
            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach(var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach(var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));
            
            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);
            
            foreach(var card in targetInfo.Hands)
                Assert.That(card.Cost, Is.EqualTo(1));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            foreach(var card in targetInfo.Hands)
                Assert.That(card.Cost, Is.EqualTo(4));
        }
        
        [Test]
        public void HandCardIncreaseHPEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardIncreaseHPEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(2));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(6));
        }
        
        [Test]
        public void HandCardDecreaseHPEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardDecreaseHPEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(2));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(1));
        }

        [Test] public void HandCardMultiplyHPEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardMultiplyHPEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(2));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(8));
        }
        
        [Test]
        public void HandCardChangeHPEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardChangeHPEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(2));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.HP, Is.EqualTo(4));
        }
        
        [Test]
        public void HandCardDecreaseDamageEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardDecreaseDamageEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(3));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(0));
        }
        
        
        [Test]
        public void HandCardIncreaseDamageEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardIncreaseDamageEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(3));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(7));
        }
        
        [Test]
        public void HandCardMultiplyDamageEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardMultiplyDamageEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(3));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(12));
        }
        
        
        [Test]
        public void HandCardChangeDamageEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new HandCardChangeDamageEffectProcessor();

            var wrongPlayerInfoContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {2, 3}, 2);
            var wrongCardContext = _gameStatus.CreateContext(1, default(Coordinate), 4, new[] {1}, 1);
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: default(Coordinate),
                value: 4,
                targetCardIDs: new[] {2, 3},
                targetUserID: 1);

            var wrongPlayerInfoResults = processor.Invoke(wrongPlayerInfoContext);
            foreach (var result in wrongPlayerInfoResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var wrongCardResults = processor.Invoke(wrongCardContext);
            foreach (var result in wrongCardResults)
                Assert.Throws<ArgumentException>(() => result.ApplyTo(_gameStatus));

            var rightResults = processor.Invoke(rightContext);

            var targetInfo = _gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == 1);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(3));

            foreach (var result in rightResults)
                result.ApplyTo(_gameStatus);

            foreach (var card in targetInfo.Hands)
                Assert.That(card.Damage, Is.EqualTo(4));
        }
        
        [Test]
        public void PassOverUnitEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new PassOverUnitEffectProcessor();
            
            var wrongSourceUnitContext = _gameStatus.CreateContext(0, new Coordinate(1, 0), default(int), null, default(int));
            var wrongTargetUnitContext = _gameStatus.CreateContext(1, new Coordinate(-1, 0), default(int), null, default(int));
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: new Coordinate(1, 0), 
                value: default(int),
                targetCardIDs: null,
                targetUserID: default(int));
            
            var wrongSourceUnitResults = processor.Invoke(wrongSourceUnitContext);
            Assert.Throws<ArgumentException>(() => wrongSourceUnitResults.GetEnumerator().MoveNext());

            var wrongTargetUnitResults = processor.Invoke(wrongTargetUnitContext);
            Assert.Throws<ArgumentException>(() => wrongTargetUnitResults.GetEnumerator().MoveNext());
            
            var rightResults = processor.Invoke(rightContext);
            
            var sourceUnit = _gameStatus.Units.FirstOrDefault(unit => unit.Coordinate == new Coordinate(0, 0));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            Assert.That(sourceUnit.Coordinate, Is.EqualTo(new Coordinate(2, 0)));
        }
        
        [Test]
        public void PushAwayUnitEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new PushAwayUnitEffectProcessor();
            
            var wrongSourceUnitContext = _gameStatus.CreateContext(0, new Coordinate(1, 0), 3, null, default(int));
            var wrongTargetUnitContext = _gameStatus.CreateContext(1, new Coordinate(-1, 0), 3, null, default(int));
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: new Coordinate(1, 0), 
                value: 3,
                targetCardIDs: null,
                targetUserID: default(int));
            
            var wrongSourceUnitResults = processor.Invoke(wrongSourceUnitContext);
            Assert.Throws<ArgumentException>(() => wrongSourceUnitResults.GetEnumerator().MoveNext());

            var wrongTargetUnitResults = processor.Invoke(wrongTargetUnitContext);
            Assert.Throws<ArgumentException>(() => wrongTargetUnitResults.GetEnumerator().MoveNext());
            
            var rightResults = processor.Invoke(rightContext);
            
            var targetUnit = _gameStatus.Units.FirstOrDefault(unit => unit.Coordinate == new Coordinate(1, 0));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            Assert.That(targetUnit.Coordinate, Is.EqualTo(new Coordinate(2, 0)));
        }
        
        [Test]
        public void SilenceTargetUnitEffectProcessorTest()
        {
            EffectProcessor<EffectContext> processor = new SilenceTargetUnitEffectProcessor();
            
            var wrongContext = _gameStatus.CreateContext(0, new Coordinate(-1, 0), default(int), null, default(int));
            var rightContext = _gameStatus.CreateContext(
                sourceUnitID: 1,
                targetCoordinate: new Coordinate(1, 0), 
                value: default(int),
                targetCardIDs: null,
                targetUserID: default(int));
            
            var wrongResults = processor.Invoke(wrongContext);
            Assert.Throws<ArgumentException>(() => wrongResults.GetEnumerator().MoveNext());
            
            var rightResults = processor.Invoke(rightContext);
            
            var targetUnit = _gameStatus.Units.FirstOrDefault(unit => unit.Coordinate == new Coordinate(1, 0));
            Assert.That(targetUnit.Abilities.Length, Is.EqualTo(1));
            
            foreach(var result in rightResults)
                result.ApplyTo(_gameStatus);
            
            Assert.That(targetUnit.Abilities.Length, Is.EqualTo(0));
        }
        
        private class TestUnit : Unit
        {
            public TestUnit(Card card, int uniqueID, int ownerID)
            {
                Origin = card;
                UniqueID = uniqueID;
                Name = card?.Name;
                OwnerID = ownerID;
            }

            public override Card Origin { get; }
            public override int UniqueID { get; }
            public override string Name { get; }
            public override int OwnerID { get; }
        }

        private class TestCard : Card
        { }

        private class TestPlayerInfo : IPlayerInfo
        {
            public TestPlayerInfo(int userID) => UserID = userID;
            
            public int UserID { get; }
            public List<Card> Deck { get; set; }
            public List<Card> Hands { get; set; }
        }
        
        private class TestGameStatus : GameStatus
        {
            public List<TestPlayerInfo> TestPlayerInfos => PlayerInfos.ConvertAll(info => info as TestPlayerInfo);
        }
    }
}