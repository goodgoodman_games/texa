﻿using Texa.Map;

namespace Texa.UDG.Models
{   
    public abstract class Unit : IReadableUnit
    {
        public abstract Card Origin { get; }
        public abstract int UniqueID { get; }
        public abstract string Name { get; }
        public int Damage { get; set; }
        public int HP { get; set; }
        public Ability[] Abilities { get; set; }
        public abstract int OwnerID { get; }
        public Coordinate Coordinate { get; set; }

        public bool IsDead => HP <= 0;

        public void TakeDamage(int value)
        {
            HP -= value;
        }
    }
}