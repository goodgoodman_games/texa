﻿using Texa.Map;

namespace Texa.UDG.Models
{
    public interface IReadableUnit
    {
        Card Origin { get; }
        int UniqueID { get; }
        string Name { get; }
        int Damage { get; }
        int HP { get; }
        Ability[] Abilities { get; }
        int OwnerID { get; }
        Coordinate Coordinate { get; }
    }
}