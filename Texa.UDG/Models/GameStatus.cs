﻿using System.Collections.Generic;
using System.Linq;
using Texa.Map;
using Texa.UDG.Effect.Context;

namespace Texa.UDG.Models
{
    public abstract class GameStatus
    {
        public List<IPlayerInfo> PlayerInfos { get; set; }
        public Grid Map { get; set; }
        public List<Unit> Units { get; set; }

        public virtual EffectContext CreateContext(int sourceUnitID, Coordinate targetCoordinate, int value, int targetUserID)
        {
            return new EffectContext
            (
                sourceUnitID: sourceUnitID,
                targetCoordinate: targetCoordinate,
                value: value,
                targetUserID: targetUserID,
                targetHands: PlayerInfos.FirstOrDefault(info => info.UserID == targetUserID)?.Hands,
                units: Units
            );
        }
    }
}