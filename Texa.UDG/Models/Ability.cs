﻿namespace Texa.UDG.Models
{
    public class Ability
    {
        public Ability(int id, int type, int value)
        {
            ID = id;
            Type = type;
            Value = value;
        }
        
        public int ID { get; }
        public int Type { get; }
        public int Value { get; }
    }
}