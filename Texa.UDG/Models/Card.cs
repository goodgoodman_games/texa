﻿namespace Texa.UDG.Models
{
    public abstract class Card : IReadableCard
    {
        public int UniqueID { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public Ability[] Abilities { get; set; }
        public int Type { get; set; }

        private int _damage;
        public int Damage { get => _damage; set => _damage = value < 0 ? 0 : value; }

        private int _hp;
        public int HP { get => _hp; set => _hp = value < 1 ? 1 : value; }

        private int _cost;
        public int Cost { get => _cost; set => _cost = value < 0 ? 0 : value; }
        
        public int Expenses { get; set; }
    }
}