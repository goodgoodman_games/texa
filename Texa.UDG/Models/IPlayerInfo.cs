﻿using System.Collections.Generic;

namespace Texa.UDG.Models
{
    public interface IPlayerInfo
    {
        int UserID { get; }
        List<Card> Deck { get; }
        List<Card> Hands { get; }
    }
}