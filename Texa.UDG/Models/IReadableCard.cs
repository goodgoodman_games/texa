﻿namespace Texa.UDG.Models
{
    public interface IReadableCard
    {
        int UniqueID { get; }
        int ID { get; }
        string Name { get; }
        int Damage { get; }
        int HP { get; }
        Ability[] Abilities { get; }
        int Type { get; }
        int Cost { get; }
        int Expenses { get; }
    }
}