﻿using Texa.Map;

namespace Texa.UDG.Effect
{
    public class EffectResultInfo
    {
        public EffectResultInfo(int targetUserID, int targetCardID, int sourceUnitID, 
            Coordinate targetCoordinate, int value, (int UniqueID, int CardID) drawCardInfo)
        {
            TargetUserID = targetUserID;
            TargetCardID = targetCardID;
            SourceUnitID = sourceUnitID;
            TargetCoordinate = targetCoordinate;
            Value = value;
            DrawCardInfo = drawCardInfo;
        }
        
        public int TargetUserID { get; }
        public int TargetCardID { get; }
        public int SourceUnitID { get; }
        public Coordinate TargetCoordinate { get; }
        public int Value { get; }
        public (int UniqueID, int CardID) DrawCardInfo { get; }
    }
}