﻿using System;
using Texa.Effect;

namespace Texa.UDG.Effect
{
    public static class EffectResultExtension
    {
        public static EffectResultInfo ToEffectResultInfo(this EffectResult self)
        {
            if(!(self.ToResultInfo() is EffectResultInfo effectResultInfo))
                throw new InvalidCastException($"{nameof(EffectResult)}가 반환하는 resultInfo의 type이 {nameof(EffectResultInfo)}가 아닙니다.");

            return effectResultInfo;
        }
    }
}