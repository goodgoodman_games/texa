﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texa.Effect;
using Texa.Map;
using Texa.UDG.Effect.Context;
using Texa.UDG.Models;

namespace Texa.UDG.Effect.Processor
{
    public class DamageTargetUnitEffectProcessor : EffectProcessor<EffectContext>
    {
        private class Result : EffectResult<GameStatus>
        {
            public Result(int value, Coordinate targetCoordinate)
            {
                _value = value;
                _targetCoordinate = targetCoordinate;
            }
            
            private readonly int _value;
            private readonly Coordinate _targetCoordinate;
            
            protected override void ApplyTo(GameStatus gameStatus)
            {
                var targetUnit = gameStatus.Units.FirstOrDefault(unit => unit.Coordinate == _targetCoordinate);
                if(targetUnit == null)
                    throw new ArgumentException($"{_targetCoordinate} 좌표를 가진 Unit이 존재하지 않습니다.");
                
                targetUnit.TakeDamage(_value);
            }
            
            public override object ToResultInfo()
            {
                return new EffectResultInfo
                (
                    targetUserID: default(int),
                    targetCardID: default(int),
                    sourceUnitID: default(int),
                    targetCoordinate: _targetCoordinate,
                    value: _value,
                    drawCardInfo: default((int, int))
                );
            }
        }
        
        public override IEnumerable<EffectResult> Invoke(EffectContext context)
        {
            var targetUnit = context.Units.SingleOrDefault(unit => unit.Coordinate == context.TargetCoordinate);
            if(targetUnit == null)
                throw new ArgumentException($"{context.TargetCoordinate}에는 유닛이 존재하지 않습니다.");
            
            yield return new Result(context.Value, targetUnit.Coordinate);
        }
    }
}