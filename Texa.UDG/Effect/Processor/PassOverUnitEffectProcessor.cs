﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texa.Effect;
using Texa.Map;
using Texa.UDG.Effect.Context;
using Texa.UDG.Models;

namespace Texa.UDG.Effect.Processor
{
    public class PassOverUnitEffectProcessor : EffectProcessor<EffectContext>
    {
        private class Result : EffectResult<GameStatus>
        {
            public Result(int sourceUnitID, Coordinate targetCoordinate)
            {
                _sourceUnitID = sourceUnitID;
                _targetCoordinate = targetCoordinate;
            }

            private readonly int _sourceUnitID;
            private readonly Coordinate _targetCoordinate;
            
            protected override void ApplyTo(GameStatus gameStatus)
            {
                var sourceUnit = gameStatus.Units.FirstOrDefault(unit => unit.UniqueID == _sourceUnitID);
                if(sourceUnit == null)
                    throw new ArgumentException($"{_sourceUnitID} id를 가진 Unit이 존재하지 않습니다.");
            
                var targetUnit = gameStatus.Units.FirstOrDefault(unit => unit.Coordinate == _targetCoordinate);
                if(targetUnit == null)
                    throw new ArgumentException($"{_targetCoordinate} 좌표를 가진 Unit이 존재하지 않습니다.");

                var map = gameStatus.Map;
                var direction = targetUnit.Coordinate - sourceUnit.Coordinate;
                var passPoint = targetUnit.Coordinate + direction;
                
                if (!map.GetCoordinates().Contains(passPoint) || gameStatus.Units.Any(unit => unit.Coordinate == passPoint))
                    throw new ArgumentException($"{passPoint}는 갈 수 없는 좌표입니다.");

                sourceUnit.Coordinate = passPoint;
            }
            
            public override object ToResultInfo()
            {
                return new EffectResultInfo
                (
                    targetUserID: default(int),
                    targetCardID: default(int),
                    sourceUnitID: _sourceUnitID,
                    targetCoordinate: _targetCoordinate,
                    value: default(int),
                    drawCardInfo: default((int, int))
                );
            }
        }
        
        public override IEnumerable<EffectResult> Invoke(EffectContext context)
        {
            var sourceUnit = context.Units.FirstOrDefault(unit => unit.UniqueID == context.SourceUnitID);
            if(sourceUnit == null)
                throw new ArgumentException($"{context.SourceUnitID} id를 가진 Unit이 존재하지 않습니다.");
            
            var targetUnit = context.Units.FirstOrDefault(unit => unit.Coordinate == context.TargetCoordinate);
            if(targetUnit == null)
                throw new ArgumentException($"{context.TargetCoordinate}에는 unit이 존재하지 않습니다.");
            
            yield return new Result(sourceUnit.UniqueID, targetUnit.Coordinate);
        }
    }
}