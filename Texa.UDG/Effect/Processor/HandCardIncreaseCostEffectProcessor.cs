﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texa.Effect;
using Texa.Map;
using Texa.UDG.Effect.Context;
using Texa.UDG.Models;

namespace Texa.UDG.Effect.Processor
{
    public abstract class HandCardIncreaseCostEffectProcessor : EffectProcessor<EffectContext>
    {
        protected class Result : EffectResult<GameStatus>
        {
            public Result(int value, int targetCardID, int targetUserID)
            {
                _value = value;
                _targetCardID = targetCardID;
                _targetUserID = targetUserID;
            }

            private readonly int _value;
            private readonly int _targetCardID;
            private readonly int _targetUserID;

            protected override void ApplyTo(GameStatus gameStatus)
            {
                var playerInfo = gameStatus.PlayerInfos.FirstOrDefault(info => info.UserID == _targetUserID);
                if (playerInfo == null)
                    throw new ArgumentException($"{_targetUserID} id를 가진 PlayerInfo가 존재하지 않습니다.");

                var targetCard = playerInfo.Hands.FirstOrDefault(card => card.UniqueID == _targetCardID);
                if (targetCard == null)
                    throw new ArgumentException($"{_targetCardID} id를 가진 Card가 존재하지 않습니다.");

                targetCard.Cost += _value;
            }
            
            public override object ToResultInfo()
            {
                return new EffectResultInfo
                (
                    targetUserID: _targetUserID,
                    targetCardID: _targetCardID,
                    sourceUnitID: default(int),
                    targetCoordinate: default(Coordinate),
                    value: _value,
                    drawCardInfo: default((int, int))
                );
            }
        }
    }
}