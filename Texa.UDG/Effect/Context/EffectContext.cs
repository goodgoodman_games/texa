﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Texa.Map;
using Texa.UDG.Models;

namespace Texa.UDG.Effect.Context
{
    public class EffectContext
    {
        public EffectContext(int sourceUnitID, Coordinate targetCoordinate, int value, int targetUserID,
            IEnumerable<IReadableCard> targetHands, IEnumerable<IReadableUnit> units)
        {
            SourceUnitID = sourceUnitID;
            TargetCoordinate = targetCoordinate;
            Value = value;
            TargetUserID = targetUserID;
            TargetHands = targetHands;
            Units = units;
        }
        
        public int SourceUnitID { get; }
        public Coordinate TargetCoordinate { get; }
        public int Value { get; }
        public int TargetUserID { get; }
        public IEnumerable<IReadableUnit> Units { get; }
        public IEnumerable<IReadableCard> TargetHands { get; }
    }
}