using Texa.Phase;
using Texa.UDG.Models;
using Texa.UDG.PhaseProcess.Phases;

namespace Texa.UDG.PhaseProcess
{
    public enum Phase
    {
        Default,
        Start,
        During,
        End
    }
    
    public class TurnPhaseProcessor: PhaseProcessor<Phase>
    {
        public TurnPhaseProcessor(GameStatus gameStatus)
        {
            Register(Phase.Start, new StartTurnPhase(gameStatus));
            Register(Phase.During, new DuringTurnPhase(gameStatus));
            Register(Phase.End, new EndTurnPhase(gameStatus));
        }
    }
}