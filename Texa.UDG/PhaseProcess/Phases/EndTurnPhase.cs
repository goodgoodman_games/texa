﻿using Texa.Phase;
using Texa.UDG.Models;

namespace Texa.UDG.PhaseProcess.Phases
{
    public class EndTurnPhase : IPhase
    {
        public EndTurnPhase(GameStatus gameStatus)
        {
        }
        
        public void OnEnterPhase() { }
        public void OnLeavePhase() { }
    }
}