﻿namespace Texa.Map.Generator
{
    public abstract class GridGenerator
    {
        public GridGenerator(GridConfig config) { Config = config; }

        public GridConfig Config { get; }

        public abstract Coordinate[] Generate();
    }
}