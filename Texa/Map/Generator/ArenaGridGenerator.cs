﻿using System.Collections.Generic;

namespace Texa.Map.Generator
{
    public class ArenaGridGenerator : GridGenerator
    {
        public ArenaGridGenerator(GridConfig config) : base(config) { }

        public override Coordinate[] Generate()
        {
            var width = Config.Width + 1;
            var height = Config.Height + 1;

            var coordinates = new List<Coordinate>();

            int negativeWidth = -(width / 2 + width % 2);
            int positiveWidth = width / 2;
            int negativeHeight = -(height / 2 + height % 2);
            int positiveHeight = height / 2;

            for (int x = negativeHeight + 1; x < positiveHeight; x++)
            {
                int z = x < 0 ? negativeWidth + 1 - x : negativeWidth + 1;
                int lastWidth = x < 0 ? positiveWidth : positiveWidth - x;

                while (z < lastWidth)
                {
                    coordinates.Add(new Coordinate(x, z));
                    z++;
                }
            }

            return coordinates.ToArray();
        }
    }
}
