﻿using System;

namespace Texa.Map
{
    [Serializable]
    public struct Coordinate : IEquatable<Coordinate>
    {
        public Coordinate(int x, int z)
        {
            X = x;
            Z = z;
        }

        public int X { get; }
        public int Y => -X - Z;
        public int Z { get; }

        public static int Distance(Coordinate from, Coordinate to)
        {
            var result = to - from;
            return (Math.Abs(result.X) + Math.Abs(result.Y) + Math.Abs(result.Z)) / 2;
        }

        public static Coordinate operator +(Coordinate left, Coordinate right) => new Coordinate(left.X + right.X, left.Z + right.Z);
        public static Coordinate operator -(Coordinate left, Coordinate right) => new Coordinate(left.X - right.X, left.Z - right.Z);

        public static bool operator ==(Coordinate left, Coordinate right) => Equals(left, right);
        public static bool operator !=(Coordinate left, Coordinate right) => !Equals(left, right);
        public bool Equals(Coordinate other) => X == other.X && Z == other.Z;

        #region Object Override

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Coordinate coordinate && Equals(coordinate);
        }


        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Z;
            }
        }

        public override string ToString() { return $"{{X : {X}, Y : {Y}, Z : {Z}}}"; }

        #endregion
    }
}