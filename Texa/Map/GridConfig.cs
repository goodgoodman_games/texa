﻿namespace Texa.Map
{
    public class GridConfig
    {
        public GridConfig(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public int Width { get; }
        public int Height { get; }
    }
}