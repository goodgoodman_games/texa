﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Texa.Map
{
    using Generator;
    
    public class Grid
    {
        private static readonly Coordinate[] DIRECTIONS =
        {
            new Coordinate(1, 0),
            new Coordinate(0, 1),
            new Coordinate(-1, 1),
            new Coordinate(1, -1),
            new Coordinate(-1, 0),
            new Coordinate(0, -1),
        };

        public Grid(GridGenerator generator)
        {
            _generator = generator ?? throw new ArgumentNullException(nameof(generator));
            _coordinates = _generator.Generate();
        }

        private readonly GridGenerator _generator;
        protected GridConfig Config => _generator.Config;

        private Coordinate[] _coordinates;

        public IEnumerable<Coordinate> GetCoordinates()
        {
            foreach (var coordinate in _coordinates)
                yield return coordinate;
        }

        public IEnumerable<Coordinate> GetNeighbor(Coordinate center)
        {
            foreach (var direction in DIRECTIONS)
            {
                var target = center + direction;
                if (_coordinates.Contains(target))
                    yield return target;
            }
        }

        public IEnumerable<Coordinate> GetTilesInRange(Coordinate center, int range)
        {
            if (range == 0)
                return new[] {center};
            if (range == 1)
            {
                var result = GetNeighbor(center).ToList();
                result.Add(center);
                return result;
            }
                
            return _coordinates.Where(coordinate => CheckTileInRange(center, coordinate, range));
        }

        private static bool CheckTileInRange(Coordinate center, Coordinate target, int range)
            => target.X <= center.X + range
               && target.X >= center.X - range
               && target.Y <= center.Y + range
               && target.Y >= center.Y - range
               && target.Z <= center.Z + range
               && target.Z >= center.Z - range;
    }
}