namespace Texa.Phase
{
    public interface IPhase
    {
        void OnEnterPhase();
        void OnLeavePhase();
    }
}