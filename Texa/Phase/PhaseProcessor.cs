using System;
using System.Collections.Generic;

namespace Texa.Phase
{
    public abstract class PhaseProcessor<TKey> : IDisposable where TKey : struct
    {
        private IPhase _currentPhase;

        private readonly Dictionary<TKey, IPhase> _phases = new Dictionary<TKey, IPhase>();
        
        protected void Register(TKey key, IPhase phase)
        {
            if(_phases.ContainsKey(key))
                throw new ArgumentException($"{key}은/는 이미 등록된 phase key입니다.");

            if(phase == null)
                throw new ArgumentNullException($"등록될 phase가 null입니다.");

            _phases.Add(key, phase);
        }

        protected void Unregister(TKey key)
        {
            if(GetCurrentPhaseKey().Equals(key))
                _currentPhase = null;
            
            if(!_phases.Remove(key))
                throw new ArgumentException($"{key}은/는 이미 해제됐거나, 등록되지 않은 phase key입니다.");
        }

        public void SetPhase(TKey key)
        {
            if(!_phases.ContainsKey(key))
                throw new ArgumentException($"{key}은/는 등록되지 않은 phase입니다.");

            _currentPhase?.OnLeavePhase();
            _currentPhase = _phases[key];
            _currentPhase.OnEnterPhase();
        }
        
        public TKey GetCurrentPhaseKey()
        {
            foreach(var key in _phases.Keys)
            {
                if(_phases[key] == _currentPhase)
                    return key;
            }
            
            return default(TKey);
        }

        public void Dispose()
        {
            _currentPhase = null;
            _phases.Clear();
        }
    }
}