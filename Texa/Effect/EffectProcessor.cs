using System.Collections.Generic;

namespace Texa.Effect
{
    public abstract class EffectProcessor<TContext>
    {
        public abstract IEnumerable<EffectResult> Invoke(TContext context);
    }
}