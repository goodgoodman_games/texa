﻿using System;

namespace Texa.Effect
{
    public abstract class EffectResult
    {
        public abstract void ApplyTo(object @object);
        public abstract object ToResultInfo();
    }

    public abstract class EffectResult<TGameStatus> : EffectResult
    {
        public override void ApplyTo(object obj)
        {
            if(!(obj is TGameStatus gameStatus))
                throw new ArgumentException($"인자가 {nameof(TGameStatus)}가 아닙니다.");
            
            ApplyTo(gameStatus);
        }

        protected abstract void ApplyTo(TGameStatus gameStatus);
    }
}